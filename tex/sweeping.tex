\subsection{Sweeping Transducers}



%%copy-pasted from another of my papers [Bose,Muscholl,Penelle,Puppis18]. We may want to modify it.
Given a word $w=a_1\dots a_n$, we denote by $\dom(w)=\{1,\dots,n\}$ its domain,
and by $w(i)$ its $i$-th letter, for any $i\in\dom(w)$.

\paragraph{Automata.}
To define two-way automata, and later two-way transducers, 
it is convenient to adopt the convention that, for any given input $w\in\Sigma^*$,
$w(0)=\lftmark$ and $w(|w|+1)=\rgtmark$, where 
$\lftmark,\rgtmark\notin\Sigma$ are special markers 
used as delimiters of the input.
In this way an automaton can detect when an endpoint 
of the input has been reached and avoid moving the head 
outside.

A \emph{two-way automaton} (\emph{\NFA} for short) 
is a tuple $\Autom=(Q,\Sigma,\Delta,I,F)$, where 
$\Sigma$ is the input alphabet,
$Q=\Qleft \cupdot \Qright$ is the 
set of states, partitioned into a set $\Qleft$ of left-reading states 
and a set $\Qright$ of right-reading states, $I\subseteq \Qright$ is 
the set of initial states, $F\subseteq Q$ is the set of final states, and
$\Delta \subseteq Q\times (\Sigma\uplus\{\lftmark,\rgtmark\})
\times Q \times \{\lft,\rgt\}$ is the transition relation. 
The partitioning of the set of states is useful for specifying which
letter is read from each state: left-reading states read the letter 
to the left, whereas right-reading states read the letter to the right.
A transition $(q,a,q',d)\in\Delta$ is \emph{leftward} (resp.~\emph{rightward})
if $d=\lft$ (resp.~$d=\rgt$). 
Of course, we assume that no leftward transition is possible when reading 
the left marker $\lftmark$, and no rightward transition is possible when 
reading the right marker $\rgtmark$.
We further restrict $\Delta$ by asking that  $(q,a,q',\lft) \in \Delta$ implies 
$q' \in \Qleft$, and $(q,a,q',\rgt) \in \Delta$ implies $q' \in\Qright$.

To define runs of \NFA we need to first introduce the
notion of configuration.
Given a \NFA $\Autom$ and a word $w\in\Sigma^*$,  
a \emph{configuration} of $\Autom$ on $w$ is a pair $(q,i)$,
with $q\in Q$ and $i\in \{1,\ldots,|w|+1\}$. 
%$i\in \{0,1,\ldots,n-1\}$. 
Such a configuration represents the fact that the automaton 
is in state $q$ and its head is {\sl between} the $(i-1)$-th 
and the $i$-th letter of $w$ (recall that we are assuming 
$w(0)=\lftmark$ and $w(|w|+1)=\rgtmark$).
%$a_{i-1}$ and $a_i$. 
The transitions that depart from a configuration 
$(q,i)$ and read $a$ are denoted $(q,i) \act{a} (q',i')$, 
and must satisfy one of the following conditions:
\begin{itemize}
\item $q \in \Qright$, $a=w(i)$, $(q,a,q',\rgt) \in\Delta$, and $i'=i+1$,
\item $q \in \Qright$, $a=w(i)$, $(q,a,q',\lft) \in\Delta$, and $i'=i$,
\item $q \in\Qleft$, $a=w(i-1)$, $(q,a,q',\rgt) \in\Delta$, and $i'=i$,
\item $q \in\Qleft$, $a=w(i-1)$, $(q,a,q',\lft) \in\Delta$, and $i'=i-1$.
\end{itemize}


A configuration $(q,i)$ on $w$ is \emph{initial} (resp.~\emph{final})
if $q\in I$ and $i=1$ 
(resp.~$q\in F$ and $i=|w|+1$). 
A \emph{run} of $\Autom$ on $w$ is a sequence 
$\rho=(q_1,i_1) \act{b_1} (q_2,i_2) \act{b_2} \cdots \act{b_m} (q_{m+1},i_{m+1})$ 
of configurations connected by transitions. 
The figure to the right
%Figure \ref{fig:twoway-run} 
depicts an input $w=a_1 a_2 a_3$ (in blue) and a possible run on it (in red),
where $q_0,q_1,q_2,q_6,q_7,q_8\in\Qright$ and $q_3,q_4,q_5\in\Qleft$,
and $1,2,3,4$ are the positions associated with the various configurations.
A run is \emph{successful} if it starts with an initial configuration 
and ends with a final configuration. 
The \emph{language} of $\Autom$ is the set $\sem{\Autom}\subseteq\Sigma^*$ of
all words on which $\Autom$ has a successful run.

When $\Autom$ has only right-reading states (i.e.~$\Qleft=\emptyset$)
and rightward transitions, we say that $\Autom$ is a \emph{one-way automaton} 
(\emph{NFA} for short).

%%Addition (inspired from [Baschenis,Gauwin,Muscholl,Puppis16]

An \NFA is said to be \emph{sweeping} if "turns" can only
occur at end markers, i.e., the following conditions are met:
\begin{itemize}
\item for every $q \in \Qright$, $(q,a,q',\lft) \in\Delta \Rightarrow a = \rgtmark$
\item for every $q \in \Qright$, $(q,a,q',\rgt) \in\Delta \Rightarrow a = \lftmark$
\end{itemize}

We say it is $k$-pass if every successful run visits every position of
the input at most $k$ times.

%%% End of small addition.

\paragraph{Transducers.}
Two-way transducers are defined similarly to two-way automata, by
introducing an output alphabet $\Gamma$ and associating an output 
from $\Gamma^*$ with each transition rule. 
So a \emph{two-way transducer} (\emph{\NFT} for short) 
$\trans=(Q,\Sigma,\Gamma,\Delta,I,F)$
is basically a \NFA as above,
 % where $Q$ is the set of states of the automata partitioned into left-moving states $\Qleft$ and the right-moving states $\Qright$, $I\subseteq Q$ is the set of initial states, $F\subseteq Q$ is the set of final states of the automata and
but with a transition relation 
$\Delta \subseteq Q\times (\Sigma\uplus\{\lftmark,\rgtmark\})
\times \Gamma^* \times Q \times \{\lft,\rgt\}$. 
A transition is usually denoted by $(q,i) \act{a|v} (q',i')$,
%$(q,a,v,q',d)\in\Delta$ 
and describes a move of the transducer from configuration $(q,i)$ 
to configuration $(q',i')$
%from state $q$ to state $q'$ 
that reads the input letter $a$ and outputs the word $v$. 
The same restrictions and conventions for two-way automata apply 
to the transitions of two-way transducers, and configurations and runs 
are defined in a similar way. 

The \emph{output} associated with a successful run
$\rho=(q_1,i_1)\act{b_1\mid v_1} (q_2,i_2) \act{b_2 \mid v_2}
\cdots \act{b_m \mid v_m} (q_{m+1},i_{m+1})$ is the word
%$\out(\r)=
$v_1 v_2 \cdots v_m\in\Gamma^*$. 
A two-way transducer $\trans$ defines a relation
 $\sem{\trans}\subseteq\Sigma^*\times\Gamma^*$ 
consisting of all the pairs $(u,v)$ such that $v$ is the output of some successful 
run $\rho$ of $\trans$ on $u$. 
%%% end copy-paste

A two-way transducer is \emph{$k$-sweeping} if its underlying
automaton is sweeping and $k$-pass.
In the rest of the paper we will concentrate
on $k$-sweeping (functional?) transducers.

\subsection{Pumping Lemma}

\begin{theorem}
Given a $\numsweep$-sweeping transducer \trans with \numstates states
and a word $u$ in $\image(\trans)$ of length at least 
$\numsweep \numstates^\numsweep$,
there exists $k' \leq \numsweep$ such that $u = u_0v_1u_1\cdots v_{k'}u_{k'}$
and for every $n\in\N$, $u_0 v_1^n u_1\cdots v_{k'}^n u_{k'} \in \image(\trans)$.
\end{theorem}

\begin{proof}
As $u \in \image(\trans)$, there exists $w \in \Sigma^*$ such that $\trans(w) = u$.

We call $\rho = (q_0,i_0) \xrightarrow{w_0\mid u_0} \cdots \xrightarrow
{w_{|w|}\mid u_{|u|}} (q_{|u|},i_{|u|})$ a run producing $u$ from $w$, and we
call $k'$ the number of sweeps of this run.
We immediately have $|u| = k' |w|$.
We have as well, for every $n = k' n' + n''$, with $0 \leq n'' < k'$,
$i_n = n''$ if $n$ is odd, and $i_n = k'-n''$ if $n$ is even.
Given $j < j' \leq k'$, we define $\rho_{j,j'}$ the subrun obtained
by keeping only configurations with a position between $j$ and $j'$,
and we call $w_{j,j'} = w_j\cdots w_{j'-1}$, and $u_{j,j'}$ the
sequence of letters produced by $\rho_{j,j'}$.
Formally, $\rho_{j,j'} = (q_j,i_j) \xrightarrow{w_j \mid u_j} \cdots
\xrightarrow{w_{j'-1} \mid u_{j'-1}} (q_{j'},i_{j'}) 
\xrightarrow{w_{j'-1} \mid u_{2k' - (j' -1)}} \cdots
\xrightarrow{w_j \mid u_{2k'-j}} (q_{2k'-j},i_{2k'-j})\cdots$.
\vp{For now, it is incorrect. Maybe formalise it with a crossing sequence?
I'll see later the right way of writing it.}

We show by induction on $|u|$ the following property:
if there is a $k'$-sweeping run on $w$ producing $u$, 
there exists $n \geq 0$ such that
$u = u_0 v_1 u_1 \cdots u_{k'n-1} v_{k'n} u_{k'n}$,
and $w = w_0 z_1 w_1 \cdots z_n w_{n}$ such that for every
$\ell \in \N$, $\trans(w_0 z_1^\ell w_0 \cdots z_n^\ell w_n) = 
u_0 v_1^\ell \cdots u_{k'n-1}v_{k'n}^\ell u_{k'n}$, with $|w_0 \cdots w_n| \leq m^{k'}$,
and a $k'$-sweeping run.

For $|u| \leq k' \numstates^{k'}$, the property is immediate, we take $n = 0$.

Suppose now the property is proved for every word of size at most $k'(n-1)$,
consider a word $u$ of size $k'n$, and take a run $\rho_{u,w}$ producing $u$
from a word $w$.
Given a position $j \leq n$, we call $s_j$ the sequence of states of $\rho_{u,w}$
that appear in a configuration with the position $j$, i.e.,
$s_j = (q_j, q_{2n - j}, q_{2n + j}, \cdots, q_{k' n + j})$.
There are $\numstates^{k'}$ different possible such sequence,
therefore, there exists at least two positions $j,j'$ such that $s_j = s_{j'}$.
Without loss of generality, we suppose we take $j,j'$ such that $j$
is the minimal position such that there is a $\ell$ with $s_j = s_\ell$, and
$j'$ the maximal position such that $s_j = s_{j'}$.

We therefore have that for every $\ell$, $\rho_{0,j} \rho_{j,j'}^\ell \rho_{j',n}$
is a valid run over $w_{0,j-1} w_{j,j'-1}^\ell w_{j',n-1}$, and
it produces a word of the form $u_0 v_1^\ell u_1 \cdots v_{k'}^\ell u_{k'}$,
with $u = u_0 v_1 u_1 \cdots v_{k'} u_{k'}$. TODO.
\vptodo{Define that notion of concatenation of runs (easy, probably 
crossing sequences)}

We call $u' = u_0 u_1 \cdots u_{k'}$, $w' = w_{0,j-1} w_{j',n-1}$. By hypothesis of induction, we know that
there is an $n \geq 0$ such that $u' = u'_0 v'_1 u'_1 \cdots v'_{k'n} u'_{k'n}$,
$w' = w'_0 z'_1 w'_1\cdots z'_{n} w'_n$ and for all $\ell$, 
$\trans(w'_0 {z'}_1^\ell w'_1 \cdots {z'}_n^\ell w'_n) =
u'_0 {v'}_1^\ell {u'}_1 \cdots {v'}_{k'n}^\ell w'_{k'n}$. 
Without loss of generality, we can assume this decomposition has been obtained
from the run $\rho_{0,j}\rho{j',n}$, and therefore, by the supposition we
made earlier, we know that there was no $p,p'$ with $p<j$ and $s_p = s_{p'}$.
Therefore, we have $w'_{0} = w_{0,j-1} w''_0 $.

Thus, we get that $w' = w_{0,j-1} w''_0 z'_1 w'_1 \cdots z'_n w'_n$,
and thus, $w = w_{0,j-1} w_{j,j'-1}w''_0 z'_1 w'_1 \cdots z'_n w'_n$.
From what precedes, we get that for every $\ell$, 
\[
\begin{tabular}{rl}
$trans(w_{0,j-1} w_{j,j'-1}^\ell w''_0 {z'}_1^\ell w'_1 \cdots {z'}_n^\ell w'_n)
=$ & $u_0 v_1^\ell (u''_0 {v'}_1^\ell u'_1 \cdots {v'}_{2n} u'''_2) v_2^\ell u_2$\\
 & $\cdots$\\
 & $u_{k'-1} v_{k'}^\ell (u''_{k'-1} {v'}_{k'(n-1)+1}^\ell u'_{k'(n-1)+1}
\cdots {v'}_{k'n} u'_{k'n})$
\end{tabular}
\]
where for every $p$, $u'''_p u_p u''_p = u'_{k'p}$, which concludes the proof.
\end{proof}
\vp{For now, that proof is quite sketchy and probably not readable, but
hopefully correct. I will try to improve it, and make it simpler.}

\vptodo{look at [BaschenisGauwinMuschollPuppis15]. They have a pumping 
lemma for runs looking supisciously close to what we are trying to achieve.}